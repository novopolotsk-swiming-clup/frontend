import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {
    private readonly MAX_PAGE_HEIGHT: number = document.body.scrollHeight;

    constructor() { }

    public ngOnInit() {
    }

    public onActivate(e, percent: number, interval: number): void {

        const scrollToBottom = window.setInterval(() => {
            const pos = window.pageYOffset;

            if ((pos * 100 / this.MAX_PAGE_HEIGHT) <= percent ) {
                window.clearInterval(scrollToBottom);
                this.onActivate(e, percent + 5, interval + 4);
            }

            if (pos > 0) {
                window.scrollTo(this.MAX_PAGE_HEIGHT, pos + 15);
            }
            if (pos === this.MAX_PAGE_HEIGHT) {
                window.clearInterval(scrollToBottom);
            }
        }, interval);
    }
}
