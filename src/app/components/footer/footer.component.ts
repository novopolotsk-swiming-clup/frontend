import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

    private readonly MAX_PAGE_HEIGHT: number = document.body.scrollHeight;
    private readonly DEFAULT_PERCENT: number = 20;

    public showArrow: boolean;
    public anHideOnStart: boolean;
    public animateClass = 'fadeInRight';
    private scrollToTop;

    constructor() { }

    public ngOnInit() {
        window.addEventListener('scroll', () => {
            const pos = window.pageYOffset;
            this.showArrow = (pos * 100 / this.MAX_PAGE_HEIGHT) > this.DEFAULT_PERCENT;

            if (this.showArrow) { this.anHideOnStart = true; }
            this.animateClass = this.showArrow ? 'fadeInRight' : 'fadeOutRight';
        }, true);
    }

    public onActivate(e, percent: number, interval: number): void {

        this.scrollToTop = window.setInterval(() => {
            const pos = window.pageYOffset;

            if ((pos * 100 / this.MAX_PAGE_HEIGHT) <= percent ) {
                window.clearInterval(this.scrollToTop);
                this.onActivate(e, percent - 5, interval + 4);
            }

            if (pos > 0) {
                window.scrollTo(0, pos - 15);
            } else {
                window.clearInterval(this.scrollToTop);
            }
        }, interval);
    }
}
