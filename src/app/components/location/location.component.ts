import { Component, OnInit } from '@angular/core';
// import { Coordinats } from 'src/app/shared/models/coordinats.model';
declare const google: any;

@Component({
    selector: 'app-location',
    templateUrl: './location.component.html',
    styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit {

    private readonly MAP_COORDINATS  = { lat: 55.5294946, lng: 28.6624603 };
    private readonly MAP_MARKER_SHOP_COORDINATS  = { lat: 55.531579, lng: 28.662164 };
    private readonly MAP_MARKER_POOL_COORDINATS  = { lat: 55.527645, lng: 28.666015 };

    constructor() { }

    public ngOnInit(): void {
        this.mapGenerate();
    }

    private mapGenerate(): void {
        const map = new google.maps.Map(document.getElementById('map'), {
            center: this.MAP_COORDINATS,
            zoom: 16,
            styles: []
        });
        this.markersInitialize(map);
    }

    private markersInitialize(map: any): void {
        const markerShop = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            position: this.MAP_MARKER_SHOP_COORDINATS
        });
        const markerPool = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            position:   this.MAP_MARKER_POOL_COORDINATS
        });
    }
}
