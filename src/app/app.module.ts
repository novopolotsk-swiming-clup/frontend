import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import 'hammerjs';

import { AppComponent } from 'src/app/app.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { AboutUsComponent } from 'src/app/components/about-us/about-us.component';
import { CaresComponent } from 'src/app/components/cares/cares.component';
import { TeamComponent } from 'src/app/components/team/team.component';
import { LocationComponent } from 'src/app/components/location/location.component';
import { FaqComponent } from 'src/app/components/faq/faq.component';
import { FooterComponent } from 'src/app/components/footer/footer.component';

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        AboutUsComponent,
        CaresComponent,
        TeamComponent,
        LocationComponent,
        FaqComponent,
        FooterComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        SharedModule,
        ReactiveFormsModule,
        HttpClientModule,
    ],
    exports: [
    ],
    providers: [
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
