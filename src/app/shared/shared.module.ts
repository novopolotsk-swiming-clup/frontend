import { NgModule } from '@angular/core';
import { CdkTableModule } from '@angular/cdk/table';
import {
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatSliderModule,
    MatTabsModule,
    MatToolbarModule,
    MatRippleModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatSortModule,
    MatTableModule,
    MatMenuModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    MatCardModule,
    MatStepperModule,
    MatPaginatorModule
} from '@angular/material';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [

    ],
    exports: [
        CdkTableModule,
        MatButtonModule,
        MatDialogModule,
        MatFormFieldModule,
        MatGridListModule,
        MatIconModule,
        MatInputModule,
        MatSelectModule,
        MatSliderModule,
        MatTabsModule,
        MatToolbarModule,
        MatRippleModule,
        MatSidenavModule,
        MatSlideToggleModule,
        MatSortModule,
        MatTableModule,
        MatMenuModule,
        MatCheckboxModule,
        MatAutocompleteModule,
        MatCardModule,
        MatStepperModule,
        FormsModule,
        MatPaginatorModule
    ]
})

export class SharedModule {
}
